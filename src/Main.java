import Jama.Matrix;
import com.sun.org.apache.regexp.internal.RE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Damian on 16.03.2018.
 */
public class Main {
    static List<String> loadedChoices;
    static List<Cryteria> loadedCryt;

    private static boolean checkAnswer(){
        Scanner scanner = new Scanner(System.in);
        String option="";
        while (!option.equals("Y") && !option.equals("N")){
            System.out.println("Podaj odpowiedź Y/N");
            option=scanner.next().toUpperCase();
        }
        return option.equals("Y");
    }

    private static List<String> insertAlternatives(int count){
        List<String> result = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        for(int i =0;i<count;i++){
            System.out.println("Podaj alternatywę");
            result.add(scanner.next());
        }
        return result;
    }

    private static Matrix fillMatrix(double[] mtx,int count){
        double[][] res = new double[count][count];
        for(int i=0;i<count;i++){
            for(int j=0;j<count;j++){
                res[i][j]=mtx[j]/mtx[i];
            }
        }
        Matrix result = new Matrix(res);
        return result;
    }

    private static Matrix insertMatrix(List<String> alternatives, int count, String cryterion){
        double[] res = new double[count];
        res[0]=1;
        Scanner scanner = new Scanner(System.in);
        for(int i=1;i<count;i++){
            System.out.println("O ile razy bardziej lepsze jest "+alternatives.get(i)+" pod wzgledem "+cryterion+" wobec "+alternatives.get(0));
            res[i]=scanner.nextFloat();
        }
        Matrix result=fillMatrix(res,count);

        return result;
    }
    private static Matrix insertMatrix(List<String> alternatives, int count, String cryterion,boolean flag, int sub_alt){
        double[] res = new double[sub_alt];
        res[0]=1;
        Scanner scanner = new Scanner(System.in);
        for(int i=1;i<sub_alt;i++){
            System.out.println("O ile razy bardziej lepsze jest "+alternatives.get(i)+" pod wzgledem "+cryterion+" wobec "+alternatives.get(0));
            res[i]=scanner.nextFloat();
        }
        Matrix result=fillMatrix(res,sub_alt);

        return result;
    }

    private static List<Cryteria> insertCryterias(int count, int alt_count,List<String> alt){
        List<Cryteria> result = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String name;
        for(int i =0;i<count;i++){
            System.out.println("Podaj nazwę kryterium");
            name=scanner.nextLine();
            if(name=="")
                name="kryterium";
            System.out.println("Czy w skład tego kryterium wejdą subkryteria?");
            boolean flag = checkAnswer();
            Cryteria crt = new Cryteria(name,alt_count,flag);
            if(flag){
                System.out.println("Podaj liczbę subkryteriów");
                int subalt_count = scanner.nextInt();
                crt.subcryterias=insertCryterias(subalt_count,alt_count,alt);
                crt.matrix=insertMatrix(alt,alt_count,name,flag,subalt_count);
            }
            else
                crt.matrix=insertMatrix(alt,alt_count,name);
            result.add(crt);



        }
    return result;
    }


    private static void showStructure(List<String>alt,List<Cryteria>crt){
        if(alt==null || crt ==null){
            System.out.println("Tu nic nie ma");
            return;
        }
        int i=1;
        for (String alter:alt) {
            System.out.println("Kryterium "+i+" : "+alter);
            i++;
        }
        if(crt==null){
            System.out.println("brak");
            return;
        }
        for (Cryteria ct:crt) {
            if(ct==null)
                System.out.println("pusto");
            else {
                ct.getInfo();
            }
        }

    }

    public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("Witaj w programie, co chcesz zrobić?");
        System.out.println("-------------------------\n");
        System.out.println("1 - Generuj plik XML");
        System.out.println("2 - Odczytaj plik XML");
        System.out.println("3 - Pokaż strukturę");
        System.out.println("4 - Oblicz wektor preferencji");
        System.out.println("0 - Wyjdź");

        selection = input.nextInt();
        return selection;
    }

    public static void create(){
        int alt_count=0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w kreatorze pliku XML do AHP, ile bierzesz pod uwagę alternatyw?");
        alt_count = scanner.nextInt();
        List<String> alternatives = insertAlternatives(alt_count);
        int crytera_count=0;
        System.out.println("Ile kryteriów bierzesz pod uwagę w kwestii decyzji?");
        crytera_count=scanner.nextInt();
        List<Cryteria>cryterias = insertCryterias(crytera_count,alt_count,alternatives);
        showStructure(alternatives,cryterias);
        String filename="";
        while(filename.length()==0) {
            System.out.println("Podaj nazwę pliku, pod jakim chcesz zapisać tą strukturę");
            filename = scanner.nextLine();
        }
        WriteXMLFile.createFile(alternatives,cryterias,filename);
    }

    public static void openExisting(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj nazwe pliku XML, ktory chcesz wczytac ");
        String path = scanner.nextLine();
        loadedChoices = ReadXMLFile.retrieveElements(path);
        loadedCryt = ReadXMLFile.retrieveCryteriaList(path);
    }

    public static int rankingMenu(){
        int selection;
        Scanner input = new Scanner(System.in);

        System.out.println("Jaka metoda chcesz to policzyc?");
        System.out.println("-------------------------\n");
        System.out.println("1 - Wektorów własnych");
        System.out.println("2 - Sredniej geometrycznej");


        selection = input.nextInt();
        return selection;
    }

    public static Matrix countByEigenvalue(Cryteria crt){
        if(crt.subcryterias==null){
            return MathOperations.showPriorityVectorByEigenvalue(crt);
        }else{
            Matrix weights = MathOperations.showPriorityVectorByEigenvalue(crt);
            List<Matrix>counted = new ArrayList<>();
            for (Cryteria subcrt:crt.subcryterias) {
                counted.add(MathOperations.showPriorityVectorByEigenvalue(subcrt));
            }
            Matrix result = new Matrix(1,crt.matrix.getColumnDimension());
            for(int i=0;i<crt.subcryterias.size();i++){
                if(counted.get(i)!=null) {
                    System.out.println(counted.get(i).getColumnDimension()+" "+counted.get(i).getRowDimension());
                    System.out.println(result.getColumnDimension()+" "+result.getRowDimension());
                    result.plusEquals(counted.get(i).times(weights.get(i, 0)));
                }
            }
            return result;
        }
    }

    public static Matrix countByGMM(Cryteria crt){
        if(crt.subcryterias==null){
            return MathOperations.showByGeometricMean(crt);
        }else{
            Matrix weights = MathOperations.showByGeometricMean(crt);
            List<Matrix>counted = new ArrayList<>();
            for (Cryteria subcrt:crt.subcryterias) {
                counted.add(MathOperations.showByGeometricMean(subcrt));
            }
            Matrix result = new Matrix(1,crt.matrix.getColumnDimension());
            for(int i=0;i<crt.subcryterias.size();i++){
                if(counted.get(i)!=null) {
                    System.out.println(counted.get(i).getColumnDimension()+" "+counted.get(i).getRowDimension());
                    System.out.println(result.getColumnDimension()+" "+result.getRowDimension());
                    result.plusEquals(counted.get(i).times(weights.get(i, 0)));
                }
            }
            return result;
        }
    }

    public static void showByEigenvalue(){
        for (Cryteria crt:loadedCryt) {
            System.out.println(Arrays.toString(countByEigenvalue(crt).getArray()));
        }
    }

    public static void showByGMM(){
        for (Cryteria crt:loadedCryt) {
            System.out.println(Arrays.toString(countByGMM(crt).getArray()));
        }
    }

    public static void showRanking(){
        int choice = rankingMenu();
        switch (choice){
            case 1: showByEigenvalue(); break;
            case 2: showByGMM(); break;
            default: System.out.println("Brak takiej funkcjonalności");
        }



    }

    public static void main(String[] args) throws IOException {

        int choice=-1;
        Scanner scanner = new Scanner(System.in);
        while(choice!=0){
            choice=menu();
            switch (choice){
                case 1: create(); break;
                case 2: openExisting(); break;
                case 3: showStructure(loadedChoices, loadedCryt); break;
                case 4: showRanking();
                case 0: System.out.println("Do widzenia!"); break;
                default: System.out.println("Brak takiej opcji"); break;
            }
        }
        /*System.out.println("Witaj w kreatorze pliku XML do AHP, ile bierzesz pod uwagę alternatyw?");
        alt_count = scanner.nextInt();
        List<String> alternatives = insertAlternatives(alt_count);
        int crytera_count=0;
        System.out.println("Ile kryteriów bierzesz pod uwagę w kwestii decyzji?");
        crytera_count=scanner.nextInt();
        List<Cryteria>cryterias = insertCryterias(crytera_count,alt_count,alternatives);
        showStructure(alternatives,cryterias);
        System.out.println("Podaj nazwę pliku, pod jakim chcesz zapisać tą strukturę");
        String filename = scanner.nextLine();
        WriteXMLFile.createFile(alternatives,cryterias,filename);
        System.out.println("Podaj nazwe pliku XML, ktory chcesz wczytac ");
        String path = scanner.nextLine();
        List<String> loadedChoices = ReadXMLFile.retrieveElements(path);
        List<Cryteria> loadedCryt = ReadXMLFile.retrieveCryteriaList(path);
        showStructure(loadedChoices,loadedCryt);*/



    }
}
