/**
 * Created by Damian on 08.04.2018.
 */
import Jama.*;
public class MathOperations {
    public static Matrix showPriorityVectorByEigenvalue(Cryteria crt){
        if(crt==null)
            return null;
        //Matrix matrix = new Matrix(mtx);
        Matrix maxEigenvector = Eigenvalue.getMaxEigenvector(crt.matrix);
        double sum = 0;
        for(double x : maxEigenvector.getArray()[0])
            sum += x;
        return maxEigenvector.times(1./sum).transpose();

    }

    public static Matrix showByGeometricMean(Cryteria crt){
        Matrix matrix = crt.matrix;
        int n = matrix.getRowDimension();
        double[] weights = new double[n];
        double[] powers = new double[n];

        double normalizationTerm = 0;
        for(int i=0; i<n; i++) {
            powers[i] = 1;
            for (int j = 0; j < n; j++)
                powers[i] *= matrix.get(i,j);
            powers[i] = Math.pow(powers[i], 1./n);
            normalizationTerm += powers[i];
        }

        for(int i=0; i<n; i++)
            weights[i] = powers[i] / normalizationTerm;
        return new Matrix(weights, 1).transpose();
    }
}
