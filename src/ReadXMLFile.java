import Jama.Matrix;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;


public class ReadXMLFile {

    public static List<String> retrieveElements(String filepath){
        List<String> result = new ArrayList<>();
        try {

            File fXmlFile = new File(filepath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("CHOICE");


            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    result.add(eElement.getTextContent());

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

            return result;
        }


        private static Cryteria retrieveCryteria(Node node){
        Cryteria result;
        if(node.getNodeType() != Node.ELEMENT_NODE) {
            System.out.println("Niewlasciwy format");
            return null;
        }
        Element element = (Element)node;
        NodeList nlist = node.getChildNodes();
        List<Cryteria> cryteriaList = new ArrayList<>();

        String name = element.getAttribute("name");
        String matrix = element.getAttribute("m");
        System.out.println(name+" "+matrix);
        Matrix mtx = Cryteria.setMatrix(matrix);
            if(nlist!=null){
                for (int i=0;i<nlist.getLength();i++) {
                    cryteriaList.add(retrieveCryteria(nlist.item(i)));
                }
                result=new Cryteria(name,mtx.getColumnDimension(),true);
                result.subcryterias = cryteriaList;
            }
            else {
                result = new Cryteria(name, mtx.getColumnDimension(), false);
            }
        result.matrix = mtx;
        return result;
        }

        public static List<Cryteria> retrieveCryteriaList(String filepath){
        List<Cryteria> result = new ArrayList<>();
            try {
                File inputFile = new File(filepath);
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(inputFile);
                doc.getDocumentElement().normalize();
                //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                //NodeList nList = doc.getChildNodes();
                NodeList nList = doc.getElementsByTagName("CRITERION");
                //System.out.println("----------------------------");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);


                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element el = (Element)nNode;
                        if(((Element) nNode).hasAttribute("m")) {
                            System.out.println("dodaje");
                            result.add(retrieveCryteria(nNode));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        return result;
        }

}

