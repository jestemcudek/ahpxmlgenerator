import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import Jama.*;

/**
 * Created by Damian on 17.03.2018.
 */
public class Cryteria {
    protected String name;
    protected Matrix matrix;
    protected List<Cryteria> subcryterias;

    public Cryteria(String name, int count,boolean flag){
        this.name=name;
        this.matrix=new Matrix(count,count);
        if(flag)
            subcryterias = new ArrayList<>();

    }

    private static double retrieveValue(String data){
        String [] tmp = data.split("/");
        double val1 = Float.parseFloat(tmp[0]);
        double val2 = Float.parseFloat(tmp[1]);
        return val1/val2;
    }

    protected String getArray(){
        double[][] show = matrix.getArrayCopy();

        return Arrays.deepToString(show);
    }

    public void getInfo(){
        System.out.println(this.name);
        System.out.println(this.getArray());
        if(this.subcryterias!=null){
            System.out.println("subkryterium "+this.name);
            for(Cryteria crt:this.subcryterias)
                if(crt!=null)
                crt.getInfo();
        }
    }

    //protected
    public static Matrix setMatrix(String m){
        String regex = "\\[|\\]";
        m = m.replaceAll(regex,"");
        m = m.replaceAll(";","");
        m = m.replaceAll(" ",",");
        String[] tmp = m.split(",\\s*");
        System.out.println(Arrays.toString(tmp));
        int size = tmp.length;
        double dim = Math.sqrt(size);
        double[][] tmpmtx = new double[(int)dim][(int)dim];

        for(int i=0;i<dim;i++){
            for(int j=0;j<dim;j++) {
                if (tmp[i*(int)dim+j].equals(""))
                    tmpmtx[i][j] = 0.f;
                if (tmp[i*(int)dim+j].contains("/")) {
                    tmpmtx[i][j] = retrieveValue(tmp[i*(int)dim+j]);
                } else {
                    tmpmtx[i][j] = Double.parseDouble(tmp[i*(int)dim+j]);
                }
            }
        }
        Matrix result = new Matrix(tmpmtx);
        return result;
    }
}
