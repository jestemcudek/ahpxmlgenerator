import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

/**
 * Created by Damian on 18.03.2018.
 */
public class WriteXMLFile {


    private static Element createElement(Cryteria crt,Document doc){
        Element result = doc.createElement("CRYTERION");
        Attr attrType = doc.createAttribute("name");
        attrType.setValue(crt.name);
        Attr attr2 = doc.createAttribute("m");
        attr2.setValue(crt.getArray());
        if(crt.subcryterias!=null){
            for (Cryteria sub:crt.subcryterias) {
                Element subresult = createElement(sub,doc);
                result.appendChild(subresult);
            }
        }
        result.setAttributeNode(attr2);
        result.setAttributeNode(attrType);

        return result;
    }

    public static void createFile(List<String> alternatives, List<Cryteria> cryterias, String filename){
        if(filename=="")
            filename="plik";
        try { DocumentBuilderFactory dbFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;

            dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();

        Element root =doc.createElement("ROOT");
        doc.appendChild(root);

        for (String alt:alternatives) {
            Element choice = doc.createElement("CHOICE");
            choice.appendChild(doc.createTextNode(alt));
            root.appendChild(choice);
        }

        for(Cryteria crt:cryterias){
            Element cryterion = createElement(crt,doc);
            root.appendChild(cryterion);
        }


        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("./"+filename+".xml").getPath());
        transformer.transform(source, result);

        // Output to console for testing
        StreamResult consoleResult = new StreamResult(System.out);
        transformer.transform(source, consoleResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
